import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 *
 * @author diwas
 */
public class Codechef {

    /**
     * @param args the command line arguments
     */
    static final int MAX = 500;

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t, n;
        try {
            t = Integer.parseInt(br.readLine());
            while (t > 0) {
                factorial(Integer.parseInt(br.readLine()));
                t--;
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    static void factorial(int n) {
        int[] res = new int[MAX];
        res[0] = 1;
        int res_size = 1;
        for (int x = 2; x <= n; x++) {
            res_size = multiply(x, res, res_size);
        }
        for (int i = res_size - 1; i >= 0; i--) {
            System.out.print(res[i]);
        }
        System.out.println();
    }

    static int multiply(int x, int res[], int res_size) {
        int carry = 0;
        for (int i = 0; i < res_size; i++) {
            int prod = res[i] * x + carry;
            res[i] = prod % 10;
            carry = prod / 10;
        }
        while (carry != 0) {
            res[res_size] = carry % 10;
            carry = carry / 10;
            res_size++;
        }
        return res_size;
    }

}